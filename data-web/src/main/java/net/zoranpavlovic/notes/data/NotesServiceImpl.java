package net.zoranpavlovic.notes.data;

import net.zoranpavlovic.notes.data.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
@Service
@Transactional
public class NotesServiceImpl implements NotesService {

    @Autowired
    private NotesJpaRepository repository;

    @Override
    public NoteJpa addNote(NoteJpa note) {
        return repository.save(note);
    }

    @Override
    public List<NoteJpa> getAll() {
        return repository.findAll();
    }
}
