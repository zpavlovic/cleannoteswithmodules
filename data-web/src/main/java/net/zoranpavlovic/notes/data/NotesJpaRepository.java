package net.zoranpavlovic.notes.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface NotesJpaRepository extends JpaRepository<NoteJpa, Integer> {

    @Query("SELECT n FROM NoteJpa n ORDER BY n.id DESC")
    List<NoteJpa> findAll();
}