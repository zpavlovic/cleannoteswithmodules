package net.zoranpavlovic.notes.data;



import net.zoranpavlovic.notes.domain.Note;
import net.zoranpavlovic.notes.domain.NotesDataSource;
import net.zoranpavlovic.notes.domain.NotesRepository;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class NotesRepositoryImpl implements NotesRepository {

    private NotesDataSource dataSource;

    public NotesRepositoryImpl(NotesDataSource dataSource){
        this.dataSource = dataSource;
    }

    @Override
    public Note addNote(Note note) {
        return dataSource.addNote(note);
    }

    @Override
    public List<Note> getAll() {
        return dataSource.getAll();
    }
}
