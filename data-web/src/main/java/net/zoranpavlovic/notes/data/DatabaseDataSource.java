package net.zoranpavlovic.notes.data;


import net.zoranpavlovic.notes.domain.Note;
import net.zoranpavlovic.notes.domain.NotesDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
@Component
public class DatabaseDataSource implements NotesDataSource {

    @Autowired
    private NotesService notesService;

    public DatabaseDataSource(){
    }

    @Override
    public Note addNote(Note note) {
        NoteJpa noteJpa = notesService.addNote(NoteMapper.getInstance().convertToJpaModel(note));
        return NoteMapper.getInstance().convertToPlainModel(noteJpa);
    }

    @Override
    public List<Note> getAll() {
        List<NoteJpa> notesJpa = notesService.getAll();
        List<Note> notes = new ArrayList<>();
        for(NoteJpa noteJpa : notesJpa){
            notes.add(NoteMapper.getInstance().convertToPlainModel(noteJpa));
        }
        return notes;
    }
}
