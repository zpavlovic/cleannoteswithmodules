package net.zoranpavlovic.notes.presentation.presenter;


import net.zoranpavlovic.notes.domain.Note;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface GetAllNotesPresenter {

    List<Note> getAll();
}
