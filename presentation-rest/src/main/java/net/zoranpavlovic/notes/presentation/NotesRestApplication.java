package net.zoranpavlovic.notes.presentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Zoran on 21/04/2017.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"net.zoranpavlovic.notes.data"})
@EntityScan(basePackages = {"net.zoranpavlovic.notes.data"})
@ComponentScan(basePackages = {"net.zoranpavlovic.notes.data", "net.zoranpavlovic.notes.presentation"})
public class NotesRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(NotesRestApplication.class, args);
    }

}
