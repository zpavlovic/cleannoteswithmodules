package net.zoranpavlovic.notes.presentation.rest;

import net.zoranpavlovic.notes.domain.Note;
import net.zoranpavlovic.notes.domain.NotesDataSource;
import net.zoranpavlovic.notes.presentation.presenter.AddNotePresenter;
import net.zoranpavlovic.notes.presentation.presenter.AddNotePresenterImpl;
import net.zoranpavlovic.notes.presentation.presenter.GetAllNotesPresenter;
import net.zoranpavlovic.notes.presentation.presenter.GetAllNotesPresenterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.List;

/**
 * Created by Zoran on 21/04/2017.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping(value = "notes/api")
public class RestController {

    private GetAllNotesPresenter getAllNotesPresenter;
    private AddNotePresenter addNotePresenter;

    @Autowired
    private NotesDataSource dataSource;

    @RequestMapping(value="/all")
    public List<Note> getAll(){
        getAllNotesPresenter = new GetAllNotesPresenterImpl(dataSource);
        return getAllNotesPresenter.getAll();
    }

    @RequestMapping(value = "/add")
    public Note addNote(@RequestParam(value="id") int id, @RequestParam(value="text") String text, @RequestParam(value="date") Date date){
        Note note = new Note(id, text, date);
        addNotePresenter = new AddNotePresenterImpl(dataSource);
        return  addNotePresenter.addNote(note);
    }
}
