package net.zoranpavlovic.notes.presentation.presenter;


import net.zoranpavlovic.notes.domain.Note;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface AddNotePresenter {
     Note addNote(Note note);
}
