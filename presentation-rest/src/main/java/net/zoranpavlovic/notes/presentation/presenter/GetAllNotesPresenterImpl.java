package net.zoranpavlovic.notes.presentation.presenter;



import net.zoranpavlovic.notes.data.NotesRepositoryImpl;
import net.zoranpavlovic.notes.domain.GetAllNotesInteractorImpl;
import net.zoranpavlovic.notes.domain.Note;
import net.zoranpavlovic.notes.domain.NotesDataSource;
import net.zoranpavlovic.notes.domain.NotesRepository;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class GetAllNotesPresenterImpl implements GetAllNotesPresenter {

    private NotesRepository repository;
    private GetAllNotesInteractorImpl interactor;

    public GetAllNotesPresenterImpl(NotesDataSource dataSource){

        repository = new NotesRepositoryImpl(dataSource);
        interactor = new GetAllNotesInteractorImpl(repository);
    }


    @Override
    public List<Note> getAll() {
        return repository.getAll();
    }
}
