package net.zoranpavlovic.notes.presentation.presenter;


import net.zoranpavlovic.notes.data.NotesRepositoryImpl;
import net.zoranpavlovic.notes.domain.*;

/**
 * Created by Zoran on 20/04/2017.
 */
public class AddNotePresenterImpl implements AddNotePresenter {

    private NotesRepository repository;
    private AddNoteInteractor interactor;

    public AddNotePresenterImpl(NotesDataSource dataSource){
        repository = new NotesRepositoryImpl(dataSource);
        interactor = new AddNoteInteractorImpl(repository);
    }

    @Override
    public Note addNote(Note note) {
        return repository.addNote(note);
    }
}
