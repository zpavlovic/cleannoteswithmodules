package net.zoranpavlovic.notes.domain;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface NotesRepository {

    Note addNote(Note note);

    List<Note> getAll();
}
