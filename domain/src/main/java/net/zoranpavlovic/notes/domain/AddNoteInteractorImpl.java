package net.zoranpavlovic.notes.domain;

/**
 * Created by Zoran on 20/04/2017.
 */
public class AddNoteInteractorImpl implements AddNoteInteractor {

    private NotesRepository repository;

    public AddNoteInteractorImpl(NotesRepository repository){
        this.repository = repository;
    }

    @Override
    public Note addNote(Note note) {
        return repository.addNote(note);
    }
}
