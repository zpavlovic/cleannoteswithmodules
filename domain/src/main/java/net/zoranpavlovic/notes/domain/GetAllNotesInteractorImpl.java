package net.zoranpavlovic.notes.domain;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class GetAllNotesInteractorImpl implements GetAllNotesInteractor {

    private NotesRepository repository;

    public GetAllNotesInteractorImpl(NotesRepository repository){
        this.repository = repository;
    }

    @Override
    public List<Note> getAll() {
        return repository.getAll();
    }
}
