package net.zoranpavlovic.notes.domain;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface AddNoteInteractor {

    Note addNote(Note note);
}
