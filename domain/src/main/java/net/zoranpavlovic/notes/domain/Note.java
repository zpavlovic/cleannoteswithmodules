package net.zoranpavlovic.notes.domain;

import java.util.Date;

/**
 * Created by Zoran on 20/04/2017.
 */
public class Note {

    private int id;

    private String text;

    private Date date;

    public Note(){

    }

    public Note(int id, String text, Date date) {
        super();
        this.id = id;
        this.text = text;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}